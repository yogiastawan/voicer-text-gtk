// #include <stdio.h>
#include <gtk/gtk.h>
#include <voicer_text_app.h>

int main(int argc, char *argv[])
{
    g_autoptr(VoicerTextApp) app = NULL;

    app = voicer_text_app_new();

    int ret = g_application_run(G_APPLICATION(app), argc, argv);
    return ret;
}
