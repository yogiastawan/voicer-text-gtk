/**
 * Copyright (C) 2022 yogiastawan
 *
 * This file is part of Voicer Text.
 *
 * Voicer Text is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voicer Text is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voicer Text.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <dashboard_window.h>
#include <recent_item.h>
#include <recent_item_object.h>

struct _DashboardWindow
{
    GtkApplicationWindow parent_instance;

    /*Template Widgets*/
    GtkHeaderBar *header_bar;
    GtkMenuButton *btn_menu;
    GtkListBox *list_recent;
};

G_DEFINE_TYPE(DashboardWindow, dashboard_window, GTK_TYPE_APPLICATION_WINDOW);

static GtkWidget *create_item(gpointer item, gpointer user_data);

static void dashboard_window_class_init(DashboardWindowClass *klass)
{
    GtkWidgetClass *w_class = GTK_WIDGET_CLASS(klass);
    gtk_widget_class_set_template_from_resource(w_class, "/com/ether/voicertext/view/dashboard-window.ui");
    gtk_widget_class_bind_template_child(w_class, DashboardWindow, header_bar);
    gtk_widget_class_bind_template_child(w_class, DashboardWindow, btn_menu);
    gtk_widget_class_bind_template_child(w_class, DashboardWindow, list_recent);
}

static void dashboard_window_init(DashboardWindow *self)
{
    gtk_widget_init_template(GTK_WIDGET(self));

    // popover menu
    GtkBuilder *builder = gtk_builder_new_from_resource("/com/ether/voicertext/menu/dashboard-popover-menu.ui");
    GtkPopoverMenu *popover_menu = GTK_POPOVER_MENU(gtk_builder_get_object(builder, "popover_menu"));
    // menumodel
    // gtk_builder_add_from_resource(builder,"/com/ether/voicertext/menu/app-menu.ui",NULL);
    // GMenuModel *menu_model = G_MENU_MODEL(gtk_builder_get_object(builder, "app-menu"));
    // gtk_popover_menu_new()
    gtk_menu_button_set_popover(self->btn_menu, GTK_WIDGET(popover_menu));

    // list model
    GListStore *list_model = g_list_store_new(RECENT_ITEM_TYPE_OBJECT);

    gtk_list_box_bind_model(self->list_recent, G_LIST_MODEL(list_model), create_item, list_model, NULL);

    // add item to list box;
    char *a = "This is Name of File";
    char *b = "This is the name of project e.g. Youtube Video 99";
    int i = 0;
    for (i = 1; i < 9; i++)
    {
        // gtk_list_box_insert(self->list_recent, GTK_WIDGET(recent_item_new("This is Name of File", "This is the name of project e.g. Youtube Video 99")), i);
        g_list_store_append(list_model, recent_item_object_new(i, a, b));
    }
}

static GtkWidget *create_item(gpointer item, gpointer user_data)
{
    RecentItemObject *obj = RECENT_ITEM_OBJECT(item);
    RecentItem *view_item = recent_item_new(NULL, NULL, G_LIST_STORE(user_data));
    g_object_bind_property(obj, "id", view_item, "id", G_BINDING_SYNC_CREATE);
    g_object_bind_property(obj, "name-file", view_item, "name-file", G_BINDING_SYNC_CREATE);
    g_object_bind_property(obj, "project-title", view_item, "project-title", G_BINDING_SYNC_CREATE);
    return GTK_WIDGET(view_item);
}