/**
 * Copyright (C) 2022 yogiastawan
 *
 * This file is part of Voicer Text.
 *
 * Voicer Text is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voicer Text is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voicer Text.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __RECENT_ITEM_H__
#define __RECENT_ITEM_H__
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define RECENT_TYPE_ITEM (recent_item_get_type())

G_DECLARE_FINAL_TYPE(RecentItem, recent_item, RECENT, ITEM, GtkListBoxRow);

RecentItem *recent_item_new(const char *name_file, const char *project_name, GListStore *list_store);

G_END_DECLS

#endif /*__RECENT_ITEM_H__*/