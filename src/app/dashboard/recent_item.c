/**
 * Copyright (C) 2022 yogiastawan
 *
 * This file is part of Voicer Text.
 *
 * Voicer Text is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voicer Text is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voicer Text.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <recent_item.h>

enum
{
    P_0, // for padding
    P_ID,
    P_NAME_FILE,
    P_PROJECT_TITLE,
    NUMB_PROPERTY
};

static GParamSpec *properties[NUMB_PROPERTY] = {
    NULL,
};

struct _RecentItem
{
    GtkListBoxRow parent_instance;

    int id;
    GtkLabel *label_name_file;
    GtkLabel *label_project_name;
    GtkButton *btn_delete_item_recent;
    GtkMenuButton *btn_item_menu;

    // GlistStore to modify
    GListStore *lstore;
};

G_DEFINE_TYPE(RecentItem, recent_item, GTK_TYPE_LIST_BOX_ROW);

static void recent_item_set_property(GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);
static void recent_item_get_property(GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void recent_item_finalize(GObject *obj);

// signal handler
static void delete_item(GtkWidget *btn, gpointer user_data);

static void recent_item_class_init(RecentItemClass *klass)
{
    GObjectClass *g_class = G_OBJECT_CLASS(klass);
    GtkWidgetClass *w_class = GTK_WIDGET_CLASS(klass);

    g_class->set_property = recent_item_set_property;
    g_class->get_property = recent_item_get_property;
    g_class->finalize = recent_item_finalize;

    gtk_widget_class_set_template_from_resource(w_class, "/com/ether/voicertext/view/recent-item-file.ui");
    gtk_widget_class_bind_template_child(w_class, RecentItem, label_name_file);
    gtk_widget_class_bind_template_child(w_class, RecentItem, label_project_name);
    gtk_widget_class_bind_template_child(w_class, RecentItem, btn_delete_item_recent);
    gtk_widget_class_bind_template_child(w_class, RecentItem, btn_item_menu);

    properties[P_ID] = g_param_spec_int("id", "Id", "Id or Position of item)", 0, G_MAXINT, 0, G_PARAM_READWRITE);
    properties[P_NAME_FILE] = g_param_spec_string("name-file", "Name File", "Name of the file", NULL, G_PARAM_READWRITE);
    properties[P_PROJECT_TITLE] = g_param_spec_string("project-title", "Project Title", "Title of the project file", NULL, G_PARAM_READWRITE);

    g_object_class_install_properties(g_class, NUMB_PROPERTY, properties);
}

static void recent_item_init(RecentItem *self)
{
    gtk_widget_init_template(GTK_WIDGET(self));

    // builder menu
    GtkBuilder *builder = gtk_builder_new_from_resource("/com/ether/voicertext/menu/recent-item-popover-menu.ui");
    GtkPopoverMenu *pop_menu = GTK_POPOVER_MENU(gtk_builder_get_object(builder, "item_popover_menu"));
    gtk_menu_button_set_popover(self->btn_item_menu, GTK_POPOVER(pop_menu));

    // signal handler
    g_signal_connect(self->btn_delete_item_recent, "clicked", G_CALLBACK(delete_item), self);
}

static void recent_item_set_property(GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    RecentItem *obj = RECENT_ITEM(object);

    switch (property_id)
    {
    case P_ID:
        obj->id = g_value_get_int(value);
        break;
    case P_NAME_FILE:
        gtk_label_set_text(obj->label_name_file, g_value_get_string(value));
        break;
    case P_PROJECT_TITLE:
        gtk_label_set_text(obj->label_project_name, g_value_get_string(value));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
        break;
    }
}
static void recent_item_get_property(GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
    RecentItem *obj = RECENT_ITEM(object);
    switch (property_id)
    {
    case P_ID:
        g_value_set_int(value, obj->id);
        break;
    case P_NAME_FILE:
        g_value_set_string(value, gtk_label_get_text(obj->label_name_file));
        break;
    case P_PROJECT_TITLE:
        g_value_set_string(value, gtk_label_get_text(obj->label_project_name));
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
        break;
    }
}
static void recent_item_finalize(GObject *obj)
{
    G_OBJECT_CLASS(recent_item_parent_class)->finalize(obj);
}

RecentItem *recent_item_new(const char *name_file, const char *project_name, GListStore *list_store)
{
    RecentItem *obj = RECENT_ITEM(g_object_new(RECENT_TYPE_ITEM, "name-file", name_file, "project-title", project_name, NULL));
    gtk_label_set_text(obj->label_name_file, name_file);
    gtk_label_set_text(obj->label_project_name, project_name);
    obj->lstore = list_store;
    return obj;
}

static void delete_item(GtkWidget *btn, gpointer user_data)
{
    g_list_store_remove(RECENT_ITEM(user_data)->lstore, RECENT_ITEM(user_data)->id);
}
