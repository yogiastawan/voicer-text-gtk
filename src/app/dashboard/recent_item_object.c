// Copyright (C) 2022 yogiastawan
//
// This file is part of Voicer Text.
//
// Voicer Text is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Voicer Text is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Voicer Text.  If not, see <http://www.gnu.org/licenses/>.

#include <recent_item_object.h>

enum
{
    p_0, // for padding
    P_ID,
    P_NAME_FILE,
    P_PROJECT_TITLE,
    NUMB_PROPERTY
};

static GParamSpec *properties[NUMB_PROPERTY] = {
    NULL,
};

struct _RecentItemObject
{
    GObject parent_instance;

    int id;
    char *name_file;
    char *project_title;
};

G_DEFINE_TYPE(RecentItemObject, recent_item_object, G_TYPE_OBJECT);

static void recent_item_object_get_property(GObject *object, guint property_id, GValue *value, GParamSpec *pspec);
static void recent_item_object_set_property(GObject *object, guint property_id, const GValue *value, GParamSpec *pspec);
static void recent_item_object_finalize(GObject *obj);

static void recent_item_object_init(RecentItemObject *obj)
{
}

static void recent_item_object_class_init(RecentItemObjectClass *klass)
{
    GObjectClass *g_class = G_OBJECT_CLASS(klass);

    g_class->set_property = recent_item_object_set_property;
    g_class->get_property = recent_item_object_get_property;
    g_class->finalize = recent_item_object_finalize;

    properties[P_ID] = g_param_spec_int("id", "Id", "Id or Position of item)", 0, G_MAXINT, 0, G_PARAM_READWRITE);
    properties[P_NAME_FILE] = g_param_spec_string("name-file", "Name File", "Name of the file", NULL, G_PARAM_READWRITE);
    properties[P_PROJECT_TITLE] = g_param_spec_string("project-title", "Project Title", "Title of the project file", NULL, G_PARAM_READWRITE);

    g_object_class_install_properties(g_class, NUMB_PROPERTY, properties);
}

static void recent_item_object_get_property(GObject *object, guint property_id, GValue *value, GParamSpec *pspec)
{
    RecentItemObject *obj = RECENT_ITEM_OBJECT(object);
    switch (property_id)
    {
    case P_ID:
        g_value_set_int(value, obj->id);
        break;
    case P_NAME_FILE:
        g_value_set_string(value, obj->name_file);
        break;
    case P_PROJECT_TITLE:
        g_value_set_string(value, obj->project_title);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
        break;
    }
}

static void recent_item_object_set_property(GObject *object, guint property_id, const GValue *value, GParamSpec *pspec)
{
    RecentItemObject *obj = RECENT_ITEM_OBJECT(object);

    switch (property_id)
    {
    case P_ID:
        obj->id = g_value_get_int(value);
        break;
    case P_NAME_FILE:
        g_free(obj->name_file);
        obj->name_file = g_value_dup_string(value);
        break;
    case P_PROJECT_TITLE:
        g_free(obj->project_title);
        obj->project_title = g_value_dup_string(value);
        break;
    default:
        G_OBJECT_WARN_INVALID_PROPERTY_ID(object, property_id, pspec);
        break;
    }
}

static void recent_item_object_finalize(GObject *obj)
{
    RecentItemObject *object = RECENT_ITEM_OBJECT(obj);
    g_free(object->name_file);
    g_free(object->project_title);
    G_OBJECT_CLASS(recent_item_object_parent_class)->finalize(obj);
}

RecentItemObject *recent_item_object_new(int id, const char *name_file, const char *project_title)
{
    RecentItemObject *obj = g_object_new(RECENT_ITEM_TYPE_OBJECT, "id", id, "name-file", name_file, "project-title", project_title, NULL);
    g_print("val id: %d || name-file: %s || project-title: %s\n", obj->id, obj->name_file, obj->project_title);
    return obj;
}