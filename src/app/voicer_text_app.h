// Copyright (C) 2022 yogiastawan
//
// This file is part of Voicer Text.
//
// Voicer Text is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Voicer Text is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Voicer Text.  If not, see <http://www.gnu.org/licenses/>.

#ifndef __VOICER_TEXT_APP_H__
#define __VOICER_TEXT_APP_H__

#include <gtk/gtk.h>

G_BEGIN_DECLS
#define VOICERTEXT_TYPE_APP (voicer_text_app_get_type())

G_DECLARE_FINAL_TYPE(VoicerTextApp, voicer_text_app, VOICERTEXT, APP, GtkApplication);

VoicerTextApp *voicer_text_app_new(void);

G_END_DECLS

#endif /*__VOICER_TEXT_APP_H__*/