/**
 * Copyright (C) 2022 yogiastawan
 *
 * This file is part of Voicer Text.
 *
 * Voicer Text is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Voicer Text is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Voicer Text.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <voicer_text_app.h>
#include <dashboard/dashboard_window.h>

struct _VoicerTextApp
{
    GtkApplication parent_instace;

    /*Window*/
    GtkWindow *window;
};

G_DEFINE_TYPE(VoicerTextApp, voicer_text_app, GTK_TYPE_APPLICATION);

static void voicer_text_app_activate(GApplication *app);

VoicerTextApp *voicer_text_app_new(void)
{
    // GtkApplication a=gtk_application_new()
    return g_object_new(VOICERTEXT_TYPE_APP, "resource_base_path", "/com/ether/voicertext", "application_id", "com.ether.voicertext", "flags", G_APPLICATION_HANDLES_OPEN, NULL);
}

static void voicer_text_app_class_init(VoicerTextAppClass *klass)
{
    G_APPLICATION_CLASS(klass)->activate = voicer_text_app_activate;
}

static void voicer_text_app_init(VoicerTextApp *self)
{
}

static void voicer_text_app_activate(GApplication *app)
{
    GtkCssProvider *provider = gtk_css_provider_new();
    gtk_css_provider_load_from_resource(provider, "/com/ether/voicertext/css/app-dark.css");
    // gtk_style_context_add_provider()
    gtk_style_context_add_provider_for_screen(gdk_screen_get_default(), GTK_STYLE_PROVIDER(provider), GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    VoicerTextApp *voT;
    g_assert(G_IS_APPLICATION(app));
    voT = VOICERTEXT_APP(app);

    voT->window = gtk_application_get_active_window(GTK_APPLICATION(app));

    if (voT->window == NULL)
    {
        voT->window = g_object_new(DASHBOARD_TYPE_WINDOW, "application", app, NULL);
    }
    gtk_window_present(voT->window);
}
